# ansible-tower

Ansible project to populate Ansible Tower with its content. Contains two main plays:

build_tower.yml - Builds and starts the container setup for AWX (not needed for Tower)

load_initial_data.yml - Loads the initial data into AWX or Ansible Tower:
 - Configure system with proper URL and http header settings
 - Remove demo content
 - Configure LDAP authentication
 - Create credentials with passwords and ssh keys
 - Define GIT projects
 - Define inventories from GIT projects
 - Define job templates from GIT projects

All towers are defined in vars/towers.yml. Configuration allows for tower-specific Ansible and Tower
versions to be defined.

Playbooks are tested in an Ansible 2.8.0 virtual environment, with ansible-tower-cli 3.3.6 installed.
